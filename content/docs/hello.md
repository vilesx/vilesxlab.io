+++
title = "keybase privacy vector"
date = "2019-11-23"
author = "vilesx"
description = "exploiting dev channel overlay"
+++

thanks to [@_noid_](https://twitter.com/_noid_) for leading that path with his great writeup on [keybase privacy](https://www.whiskey-tango.org/2019/11/keybase-weve-got-privacy-problem.html).  in the spirit of responsible disclosure i'd like to present to you a known privacy issue that receives little to no attention.

##### keybase background
 keybase (keybase.io) is a platform designed to help users send and receive encrypted communications as well as securely store files and collaborate with others in “teams”. keybase also allows users to attest to ownership of other accounts on sites such as github, twitter, mastadon, personal websites, as well as validating cryptocurrency addresses. keybase was founded by chris coyne and max krohn. 

##### issue description
 keybase (keybase.io) is a platform designed to help users send and receive encrypted communications as well as securely store files and collaborate with others in “teams”. keybase also allows users to attest to ownership of other accounts on sites such as github, twitter, mastadon, personal websites, as well as validating cryptocurrency addresses. keybase was founded by chris coyne and max krohn. 

##### issue impact
 keybase (keybase.io) is a platform designed to help users send and receive encrypted communications as well as securely store files and collaborate with others in “teams”. keybase also allows users to attest to ownership of other accounts on sites such as github, twitter, mastadon, personal websites, as well as validating cryptocurrency addresses. keybase was founded by chris coyne and max krohn. 

##### proof of concept
 keybase (keybase.io) is a platform designed to help users send and receive encrypted communications as well as securely store files and collaborate with others in “teams”. keybase also allows users to attest to ownership of other accounts on sites such as github, twitter, mastadon, personal websites, as well as validating cryptocurrency addresses. keybase was founded by chris coyne and max krohn. 



keybase is a chat system that encourages users to onramp to cryptography.  it's an interesting community and a nice bit of tech kit.  originating in 2014 it has gone through numerous iterations and countless development cycles to get where it is today.

without digging into the core codebase i'd like to discuss a feature that is not an exploit but is used for developer testing which can be `abused` as an overlay within a team or direct message.

#####