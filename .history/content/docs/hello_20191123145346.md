+++
title = "keybase privacy vector"
date = "2019-11-23"
author = "vilesx"
cover = "vilesx.jpg"
description = "exploiting dev channel overlay"
+++

thanks to [@_noid_](https://twitter.com/_noid_) for leading that path with his great writeup on [keybase privacy](https://www.whiskey-tango.org/2019/11/keybase-weve-got-privacy-problem.html).  in the spirit of responsible disclosure i'd like to present to you a known privacy issue that receives little to no attention.

keybase is a chat system that encourages users to onramp to cryptography.  it's an interesting community and a nice bit of tech kit.  originating in 2014 it has gone through numerous iterations and countless development cycles to get where it is today.